import { Form } from './components/Form'

function App() {
  return (
    <div>
      <h1>Calculadora de tinta</h1>

      <Form />
    </div>
  )
}

export default App
