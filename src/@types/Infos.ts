export type InfosModal = {
  wallWidth: number
  wallHeight: number
  doorHeight: number
  doorWidth: number
  windowHeight: number
  windowWidth: number
}
