import styled from 'styled-components'

export const Container = styled.div`
  position: fixed;
  z-index: 999;
  width: 100vw;
  height: 100vh;
  inset: 0;
  background: rgba(0, 0, 0, 0.75);
  .content {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    @media (max-width: 540px) {
      width: 100%;
    }
  }
`

export const Box = styled.div`
  width: 600px;
  height: 400px;
  background: ${(props) => props.theme['base-form']};
  box-shadow: 0px 4px 4px ${(props) => props.theme['base-form']};
  border-radius: 8px;
  padding: 32px 40px;

  @media (max-width: 540px) {
    width: 100%;
  }

  .close {
    display: flex;
    justify-content: flex-end;

    button {
      background: transparent;
      border: none;
      cursor: pointer;
    }
  }

  .content-infos {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;

    img {
      width: 200px;
    }
  }
`
