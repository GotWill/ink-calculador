import { X } from '@phosphor-icons/react'
import * as C from './styles'
import Img from '../../assets/lata.png'
import { InfosModal } from '../../@types/Infos'

type ModalProps = {
  close: () => void
  data: InfosModal
}

export function Modal({ close, data }: ModalProps) {
  const {
    doorHeight,
    doorWidth,
    wallHeight,
    wallWidth,
    windowHeight,
    windowWidth,
  } = data

  const areaTotalWall = wallHeight * wallWidth
  const areaTotalDorr = doorWidth * doorHeight
  const areaTotalWindow = windowWidth * windowHeight

  const areaToPaint = areaTotalWall - areaTotalDorr - areaTotalWindow

  const paintCans = [18, 3.6, 2.5, 0.5]

  let quantityTotalInk = areaToPaint / 10

  const paintCansObj: { [key: number]: number } = {
    18: 0,
    3.6: 0,
    2.5: 0,
    0.5: 0,
  }

  // calcula a quantidade de latas necessárias para cada capacidade
  for (const capacity of paintCans) {
    const quantityCans = Math.floor(quantityTotalInk / capacity)

    if (quantityCans > 0) {
      paintCansObj[capacity] = quantityCans
      quantityTotalInk -= quantityCans * capacity
    }
  }

  let message = `Para pintar uma area de ${areaToPaint}m voce precisara de`

  for (const [capacity, amount] of Object.entries(paintCansObj)) {
    if (amount > 0) {
      message += ` ${amount} lata(s) de ${capacity}`
    }
  }

  return (
    <C.Container>
      <div className="content">
        <C.Box>
          <div className="close">
            <button onClick={close}>
              <X size={30} color="#fff" />
            </button>
          </div>

          <div className="content-infos">
            <img src={Img} alt="" />
            <h2>{message}</h2>
          </div>
        </C.Box>
      </div>
    </C.Container>
  )
}
