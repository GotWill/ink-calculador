import { useState } from 'react'
import * as C from './styles'
import { ArrowRight, ArrowUUpLeft, Calculator } from '@phosphor-icons/react'
import { Modal } from '../Modal'
import * as z from 'zod'
import { zodResolver } from '@hookform/resolvers/zod'
import { useForm } from 'react-hook-form'
import { InfosModal } from '../../@types/Infos'
import { AlertError } from '../AlertError'

export function Form() {
  const [step, setStep] = useState<number>(1)
  const [modal, setModal] = useState(false)
  const [alertError, setAlertError] = useState('')
  const [formValues, setFormValues] = useState({} as InfosModal)

  const schema = z.object({
    wallWidth: z.number(),
    wallHeight: z.number(),
    doorHeight: z.number(),
    doorWidth: z.number(),
    windowHeight: z.number(),
    windowWidth: z.number(),
  })

  type SearchFormInputs = z.infer<typeof schema>

  const { handleSubmit, register, watch, reset } = useForm<SearchFormInputs>({
    resolver: zodResolver(schema),
  })

  const {
    wallWidth,
    wallHeight,
    doorHeight,
    doorWidth,
    windowHeight,
    windowWidth,
  } = watch()

  function submitForm(data: SearchFormInputs) {
    const {
      wallWidth,
      wallHeight,
      doorHeight,
      doorWidth,
      windowHeight,
      windowWidth,
    } = data

    const valueTotalDoor = doorHeight + doorWidth
    const valueTotalWindow = windowHeight + windowWidth
    const valueTotalWall = wallHeight + wallWidth

    if (
      wallWidth <= 0 ||
      wallWidth > 50 ||
      wallHeight <= 0 ||
      wallHeight > 50
    ) {
      setAlertError('As medidas das paredes devem estar entre 1 e 50 metros.')
      return
    }

    if (valueTotalWindow + valueTotalDoor >= valueTotalWall * 2) {
      setAlertError(
        'A área das portas e janelas não pode ser maior do que 50% da área das paredes.',
      )
      return
    }

    setAlertError('')
    setFormValues(data)
    setModal(true)
    reset()
    setStep(1)
  }

  return (
    <>
      {modal === true && (
        <div>
          <Modal data={formValues} close={() => setModal(false)} />
        </div>
      )}

      {alertError !== '' && (
        <AlertError value={alertError} close={() => setAlertError('')} />
      )}

      <C.Container>
        <form onSubmit={handleSubmit(submitForm)}>
          {step === 1 && (
            <>
              <C.FormGroup>
                <label htmlFor="">Largura da Parede</label>
                <input
                  type="number"
                  {...register('wallWidth', { valueAsNumber: true })}
                />
              </C.FormGroup>

              <C.FormGroup>
                <label htmlFor="">Altura da Parede</label>
                <input
                  type="number"
                  {...register('wallHeight', { valueAsNumber: true })}
                />
              </C.FormGroup>

              <button
                disabled={!wallWidth || !wallHeight}
                onClick={() => setStep(2)}
              >
                Próximo passo <ArrowRight size={24} />
              </button>
            </>
          )}

          {step === 2 && (
            <>
              <C.FormGroup>
                <label htmlFor="">Largura da Porta</label>
                <input
                  type="number"
                  {...register('doorWidth', { valueAsNumber: true })}
                />
              </C.FormGroup>

              <C.FormGroup>
                <label htmlFor="">Altura da Porta</label>
                <input
                  type="number"
                  {...register('doorHeight', { valueAsNumber: true })}
                />
              </C.FormGroup>
              <div className="div-step">
                <button onClick={() => setStep(1)} className="btn-back">
                  Voltar <ArrowUUpLeft size={24} />
                </button>
                <button
                  disabled={!doorWidth || !doorHeight}
                  onClick={() => setStep(3)}
                >
                  Próximo passo <ArrowRight size={24} />
                </button>
              </div>
            </>
          )}

          {step === 3 && (
            <>
              <C.FormGroup>
                <label htmlFor="">Largura da Janela</label>
                <input
                  type="number"
                  {...register('windowWidth', { valueAsNumber: true })}
                />
              </C.FormGroup>
              <C.FormGroup>
                <label htmlFor="">Altura da Janela</label>
                <input
                  type="number"
                  {...register('windowHeight', { valueAsNumber: true })}
                />
              </C.FormGroup>

              <div className="div-step">
                <button onClick={() => setStep(2)} className="btn-back">
                  Voltar <ArrowUUpLeft size={24} />
                </button>
                <button disabled={!windowWidth || !windowHeight} type="submit">
                  {' '}
                  Calcular <Calculator size={24} />
                </button>
              </div>
            </>
          )}
        </form>
      </C.Container>
    </>
  )
}
