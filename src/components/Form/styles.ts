import styled from 'styled-components'

export const Container = styled.div`
  margin-top: 3rem;
  width: 540px;
  height: 290px;
  background: ${(props) => props.theme['base-form']};
  border: 1px solid ${(props) => props.theme['base-bnt-back']};
  border-radius: 6px;
  padding: 24px;

  @media (max-width: 540px) {
    width: 100%;
  }

  button {
    background: ${(props) => props.theme['base-green']};
    border-radius: 6px;
    padding: 12px;
    margin: 15px 0;
    border: none;
    cursor: pointer;
    color: #ffffff;
    font-weight: 500;
    font-size: 14px;
    width: 100%;
    height: 46px;
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 8px;

    &:not(:disabled):hover {
      background-color: ${(props) => props.theme['base-green-hover']};
      animation: 2s ease-in-out;
    }

    &:disabled {
      cursor: not-allowed;
      opacity: 0.2;
    }
  }

  .div-step {
    display: flex;
    gap: 30px;

    .btn-back {
      background: ${(props) => props.theme['base-bnt-back']};

      &:hover {
        background-color: ${(props) => props.theme['base-bnt-back']};
      }
    }
  }
`

export const FormGroup = styled.div`
  display: flex;
  flex-direction: column;
  align-items: baseline;
  gap: 10px;
  margin: 10px 0;

  label {
    color: ${(props) => props.theme['base-label']};
    font-size: 14px;
  }

  input {
    background: ${(props) => props.theme['bg-color']};
    border: none;
    color: #7c7c8a;
    width: 100%;
    height: 46px;
    border-radius: 6px;
    padding: 12px 16px;
    outline: none;
    font-size: 18px;
  }

  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`
