import { X } from '@phosphor-icons/react'
import * as C from './styles'

type InfoAlertProps = {
  value: string
  close: () => void
}

export function AlertError({ close, value }: InfoAlertProps) {
  return (
    <C.Container>
      <div className="flex">
        {value}
        <button onClick={close}>
          <X size={28} color="#121214" />
        </button>
      </div>
    </C.Container>
  )
}
