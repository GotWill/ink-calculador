import styled from 'styled-components'

export const Container = styled.div`
  position: absolute;
  color: ${(props) => props.theme['color-alert']};
  background-color: ${(props) => props.theme['base-alert']};
  border: 1px solid darken(#feefb3, 15%);
  padding: 12px;
  border-radius: 5px;
  top: 5rem;
  left: 50%;
  transform: translate(-50%, -50%);

  @media (max-width: 600px) {
    width: 90%;
  }

  .flex {
    display: flex;
    align-items: center;
    gap: 10px;

    button {
      background: transparent;
      border: none;
      cursor: pointer;
    }
  }
`
