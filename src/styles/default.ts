export const defaultTheme = {
  'base-alert': '#FEEFB3',
  'color-alert': '#9f6000',
  'base-green': '#00875F',
  'base-green-hover': '#00b37e',
  'base-label': '#e1e1e6',
  'bg-color': '#121214',
  'base-form': '#202024',
  'base-bnt-back': '#323238',
}
