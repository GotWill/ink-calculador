# Calculadora de Tinta

## Como rodar

Clonar o projeto com a seguinte url <br>
https://gitlab.com/GotWill/ink-calculador.git

Instale as dependências com o seguinte comando:

npm install

Após a instalação das dependências, inicie a aplicação com o comando:

npm run dev

Acesse a aplicação no seu navegador em http://localhost:3000.
